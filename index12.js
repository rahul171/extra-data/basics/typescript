// type A = 'hello' | 'Hello';
//
// const b: A = 'Hello';
function Abcd(a) {
    return a.length;
}
var a = Abcd('aa');
var aa = Abcd(['aa', 'bb', 'cc']);
console.log(a);
console.log(aa);
