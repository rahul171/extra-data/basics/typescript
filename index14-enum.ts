enum A {
    name,
    value,
    a = 's1',
    b = 's2',
    c = 's3'
}

const a: A = A.value;

console.log(a);

enum Aa {
    a=2,
    b,
    c,
    d='hello',
    e=a
}

console.log(Aa.a);
console.log(Aa.e);
console.log(Aa.a === Aa.e);

enum B {
    a,
    // b= 'abcd'.length,
    c=1,
    d=0,
    // e=c|d,
    // f=Aa.e
}

console.log(B);
console.log(B.d === B.a);
// console.log(B.e === B.c);

interface Abcd {
    a: Aa.a,
    b: B.d
}


enum Bb {
    a=1,
    b=3
}

const bb1 = (x: Bb) => {
    if (x !== Bb.a || x !== Bb.b) {

    }
}
