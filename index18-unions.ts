interface A {
    aa(): boolean;
    bb(): boolean;
}

interface B {
    aa(): boolean;
    cc(): number;
}

declare function a(): A | B;

// a.aa();



// interface Abcd {
//     a: () => boolean;
// }

// function a(): Abcd {
//     return 'something';
// }

// const a: Abcd = () => {
//
// };

// a.a = () => {
//     return true;
// }
