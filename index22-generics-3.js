var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var A = /** @class */ (function () {
    function A() {
    }
    return A;
}());
var M1 = /** @class */ (function () {
    function M1() {
    }
    return M1;
}());
var M2 = /** @class */ (function () {
    function M2() {
    }
    return M2;
}());
var B1 = /** @class */ (function (_super) {
    __extends(B1, _super);
    function B1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return B1;
}(A));
var B2 = /** @class */ (function (_super) {
    __extends(B2, _super);
    function B2() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return B2;
}(A));
function createInstance(arg) {
    return new arg();
}
var b1 = createInstance(B1);
var b2 = createInstance(B2);
console.log(b1);
console.log(b2);
