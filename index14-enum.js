var A;
(function (A) {
    A[A["name"] = 0] = "name";
    A[A["value"] = 1] = "value";
    A["a"] = "s1";
    A["b"] = "s2";
    A["c"] = "s3";
})(A || (A = {}));
var a = A.value;
console.log(a);
var Aa;
(function (Aa) {
    Aa[Aa["a"] = 2] = "a";
    Aa[Aa["b"] = 3] = "b";
    Aa[Aa["c"] = 4] = "c";
    Aa["d"] = "hello";
    Aa[Aa["e"] = 2] = "e";
})(Aa || (Aa = {}));
console.log(Aa.a);
console.log(Aa.e);
console.log(Aa.a === Aa.e);
var B;
(function (B) {
    B[B["a"] = 0] = "a";
    // b= 'abcd'.length,
    B[B["c"] = 1] = "c";
    B[B["d"] = 0] = "d";
    // e=c|d,
    // f=Aa.e
})(B || (B = {}));
console.log(B);
console.log(B.d === B.a);
var Bb;
(function (Bb) {
    Bb[Bb["a"] = 1] = "a";
    Bb[Bb["b"] = 3] = "b";
})(Bb || (Bb = {}));
var bb1 = function (x) {
    if (x !== Bb.a || x !== Bb.b) {
    }
};
var Aa1;
(function (Aa1) {
    Aa1[Aa1["a"] = 1] = "a";
    Aa1[Aa1["b"] = 22] = "b";
})(Aa1 || (Aa1 = {}));
var Bb1;
(function (Bb1) {
    Bb1[Bb1["a"] = 1] = "a";
    Bb1[Bb1["b"] = 2] = "b";
})(Bb1 || (Bb1 = {}));
console.log(Aa1.a === Bb1.a);
// const abcd1 = (x: Bb1) => {
//     if (x !== Aa1.a || x !== Bb1.b) {
//
//     }
// }
