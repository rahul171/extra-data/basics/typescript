class A {}
A.a = 1;
class B {}
B.b = 2;

Object.setPrototypeOf(A, B);

console.log(A);
console.log(A.b);

class C {
    constructor() {
        this.msg = 'hello';
    }
}

const a = new A();

C.call(a);

debugger;
