const add: (x: number, y: string) => string = (a: number, b: string): string => {
    return a + b;
}

const b = add(11, '22');

const add1 = (a: number, b: string): string => {
    return a + b;
}

const b1 = add1(11, '22');

const add2: (x: number, y: string) => string = (a, b) => {
    return a + b;
}

const b2 = add2(11, '22');
