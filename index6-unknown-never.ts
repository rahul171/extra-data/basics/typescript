class UserAccount {
    name: string = 'dd';
    private id: string;

    constructor(name: string, id: number) {
        // this.name = name + 'hello';
        // this.id = id + 11;
    }
}

interface Rs {
    d: string,
    b: number
}

const jsonParser = (jsonString: string): any => JSON.parse(jsonString);

const obj = jsonParser(`{"a": "b"}`) as Rs;

console.log(obj.d);

const neverReturns = () => {
    throw new Error('something');
};

const validateUser = (user: Rs) => {
    if (user) {
        console.log('in here');
        return user.d;
    }

    neverReturns();
}

const a: Rs = {
    d: 'hello there',
    b: 11
};

const res = validateUser(a);

console.log(res);

const res1 = validateUser(jsonParser(`null`));

console.log(res1);

console.log(typeof JSON.parse(null))
console.log(typeof JSON.parse('null'))

console.log(typeof null);
console.log(typeof 'null');

enum Something {
    someone = 'so',
    hey = 'hy',
    There = 'tr',
    extra = 'ext'
}

const getVal = (something: Something) => {
    switch(something) {
        case Something.someone:
            return 'someone str';
        case Something.hey:
            return 'hey str';
        case Something.There:
            return 'there str';

        default:
            const df: never = something;
            return df;
    }
}

const val = getVal(Something.extra);

type isNever = string | number | never;
