// type A = 'hello' | 'Hello';
//
// const b: A = 'Hello';

const A = (a: string) => {
    return a.length;
}

const AA = (a: string[]) => {
    return a.length;
}

function Abcd(a: string | string[]): number {
    if (typeof a === 'string') {
        // try this outside if condition.
        const b = A(a);
    }
    if (Array.isArray(a)) {
        const c = AA(a);
    }
    // const d = A(a);
    // const e = AA(a);
    return 11;
}

const a = Abcd('aa');
const aa = Abcd(['aa', 'bb', 'cc']);

console.log(a);
console.log(aa);
