function A<T>(arg: T): T {
    return arg;
}

// check types of both a and a1 variables. (hover)
const a = A<string>('fdfd');
const a1 = A('fdfd');

function AA<T>(arg: T[]): T[] {
    return [...arg];
}

const aa = AA([1,2,3]);
const aa1 = AA(['abcd', 'abcd']);
const aa2 = AA([1, 'abcd', true, [1,2,3,4], ()=>{}, function(){console.log('hello');}]);


const b: <T>(a: T) => T = <T>(arg: T) => {
    return arg;
}

const b1 = b<number>(11);

interface C {
    a: number;
    b: boolean
}

const c1: () => C = () => {
    return {
        a: 1,
        b: true
    };
}

const c3: { (): C } = () => {
    return {
        a: 1,
        b: true
    };
}

interface D {
    (a: number, b: number): boolean;
}

const c4: D = (x, y) => {
    return true;
}

const c5: { (a: number, b: number): true } = (x, y) => {
    return (():true => {return true;})();
}

