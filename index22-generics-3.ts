class A {
    a: string;
}

class M1 {
    m1: string;
}

class M2 {
    m2: string;
}

class B1 extends A {
    b1: M1;
}

class B2 extends A {
    b2: M2;
}

// what is the difference between this and the function below this?
// function createInstance<T extends A>(arg: { new (): T }): T {
//     return new arg();
// }

function createInstance<T>(arg: { new (): T }): T {
    return new arg();
}

const b1 = createInstance(B1);
const b2 = createInstance(B2);

b1.b1.m1;
b2.b2.m2;
