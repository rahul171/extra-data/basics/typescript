interface A<Type> {
    methodA: (a: Type) => boolean;
    methodB: () => Type;
    varC: Type | number;
    varD: unknown;
}

const a: A<string> = {
    methodA: (a) => {
        return true;
    },
    methodB: () => {
        return 'aa';
    },
    varC: 33,
    varD: 'ss'
};

const b = a.methodA('22');
const d = a.varD as string;

console.log(b);
console.log(d);
