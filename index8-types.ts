type Rs = {
    a: number;
    b: number;
}

type A = {
    p: (x: number, y: number, z?: undefined) => Rs;
}

type AA = A & {
    q: (x: string, y: boolean) => boolean;
};

type AAA = AA & {
    a: number,
    b: string
};

const a: AAA = {
    p: (a, b, c) => {
        return { a: 11, b: 22 };
    },
    q: (a, b) => {
        return true;
    },
    a: 11,
    b: 'hello'
};

const val = a.p(1, 222222);
