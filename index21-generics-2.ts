// function A<T>(arg: T): number {
//     return arg.length;
// }

interface LenInterface {
    length: number;
}

function AA<T extends LenInterface>(arg: T): number {
    return arg.length;
}

// both are same, just wanted to see the datatype of [] and see if it has length in typescript stubs.
const aa = AA([0,1]);
const aa1 = AA<Array<number>>([0,1]);

interface KeyofInterface {
    a: number,
    b: string,
    c: boolean
}

const aa2: keyof KeyofInterface = 'a';

function AAA<T, K extends keyof T>(obj: T, key: K): T {
    return obj;
}

type Bb = "a" | "b" | "c";

// try changing this to 'c', the below function call will show the error.
const aaa1: Bb = 'b';

const aaa = AAA({a:1, b:2}, aaa1);

interface PropertyInterface {
    abcd: () => void;
}

function AAAA<T extends PropertyInterface, K extends keyof T>(obj: T, key: K) {
    obj.abcd();
    return obj[key];
}

function Create() {
    this.a = 1;
    this.b = 2;
}

Create.prototype.abcd = function() {
    console.log(`Hello: ${this.a}`);
}

// not safe, because type of c is any.
// typescript not able to detect if there is a function abcd() on c or not.
// might result into runtime error if the function is not there.
const c = new Create();

console.log(c);
c.abcd();

const aaaa = AAAA(c, 'a');

console.log(aaaa);

// safe
const d: {a:number, b:number, abcd:()=>void} = new Create();

console.log(d);
d.abcd();

const aaaa1 = AAAA(d, 'a');

console.log(aaaa1);
