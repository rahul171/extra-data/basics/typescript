var UserAccount = /** @class */ (function () {
    function UserAccount(name, id) {
        this.name = 'dd';
        // this.name = name + 'hello';
        // this.id = id + 11;
    }
    return UserAccount;
}());
var jsonParser = function (jsonString) { return JSON.parse(jsonString); };
var obj = jsonParser("{\"a\": \"b\"}");
console.log(obj.d);
var neverReturns = function () {
    throw new Error('something');
};
var validateUser = function (user) {
    if (user) {
        console.log('in here');
        return user.d;
    }
    neverReturns();
};
var a = {
    d: 'hello there',
    b: 11
};
var res = validateUser(a);
console.log(res);
var res1 = validateUser(jsonParser("null"));
console.log(res1);
console.log(typeof JSON.parse(null));
console.log(typeof JSON.parse('null'));
console.log(typeof null);
console.log(typeof 'null');
var Something;
(function (Something) {
    Something["someone"] = "so";
    Something["hey"] = "hy";
    Something["There"] = "tr";
    Something["extra"] = "ext";
})(Something || (Something = {}));
var getVal = function (something) {
    switch (something) {
        case Something.someone:
            return 'someone str';
        case Something.hey:
            return 'hey str';
        case Something.There:
            return 'there str';
        default:
            var df = something;
            return df;
    }
};
var val = getVal(Something.extra);
