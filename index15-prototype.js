function A() {
    this.a = 11;
    this.b = 22;
}
A.prototype.methodA = function () {
    console.log(this.a);
};
var a = new A();
a.methodA();
var num = 10.10;
console.log(num.toFixed(1));
