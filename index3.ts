class A {
    constructor(a: string) {}

    halo() {
        console.log('halo halo');
    }
}

class B extends A {
    constructor() {
        super('11');
    }
    halo() {
        console.log('aaaa');
    }
}

// const b = new B();
