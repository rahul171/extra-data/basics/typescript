interface Rs {
    a: number;
    b: number;
    methodA: () => void;
}

function A() {
    this.a = 11;
    this.b = 22;
}

A.prototype.methodA = function() {
    console.log(this.a);
};

const a: Rs = new A();

a.methodA();

const num: number = 10.10;

console.log(num.toFixed(1));
