interface A {
    a: number;
    b?: string;
}

const a: A = { a: 1, b: 'ss' };

class B implements A {
    a: number;
    // b: string;
    static e: number;

    constructor(private c: number = 66, private d?: string) {}
}

const b = new B();
B.e = 56;
console.log(b);
console.log(B);
console.log(B.e);
