// a.aa();
// interface Abcd {
//     a: () => boolean;
// }
// function a(): Abcd {
//     return 'something';
// }
// const a: Abcd = () => {
//
// };
// a.a = () => {
//     return true;
// }
var Aa;
(function (Aa) {
    Aa[Aa["a"] = 2] = "a";
    Aa[Aa["b"] = 3] = "b";
    Aa[Aa["c"] = 4] = "c";
    Aa["d"] = "hello";
    Aa[Aa["e"] = 2] = "e";
})(Aa || (Aa = {}));
console.log(Aa.a);
console.log(Aa.e);
console.log(Aa.a === Aa.e);
