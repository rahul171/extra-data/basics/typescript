type A = {
    (x: number, y: number, z?: undefined): boolean;
}

type AA = A & {
    q: (x: string, y: boolean) => boolean;
};

const a: A = (a, b, c) => {
    return true;
};

const aa: AA = (a, b, c) => {
    return true;
};

aa.q = (a, b) => {
    return false;
}
