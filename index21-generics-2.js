// function A<T>(arg: T): number {
//     return arg.length;
// }
function AA(arg) {
    return arg.length;
}
// both are same, just wanted to see the datatype of [] and see if it has length in typescript stubs.
var aa = AA([0, 1]);
var aa1 = AA([0, 1]);
var aa2 = 'a';
function AAA(obj, key) {
    return obj;
}
// try changing this to 'c', the below function call will show the error.
var aaa1 = 'b';
var aaa = AAA({ a: 1, b: 2 }, aaa1);
function AAAA(obj, key) {
    obj.abcd();
    return obj[key];
}
function Create() {
    this.a = 1;
    this.b = 2;
}
Create.prototype.abcd = function () {
    console.log("Hello: " + this.a);
};
// not safe, because type of c is any.
// typescript not able to detect if there is a function abcd() on c or not.
// might result into runtime error if the function is not there.
var c = new Create();
console.log(c);
c.abcd();
var aaaa = AAAA(c, 'a');
console.log(aaaa);
// safe
var d = new Create();
console.log(d);
d.abcd();
var aaaa1 = AAAA(d, 'a');
console.log(aaaa1);
