enum Aa1 {
    a=1,
    b=22
}

enum Bb1 {
    a=1,
    b=2
}

console.log(Aa1.a === Bb1.a);

// const abcd1 = (x: Bb1) => {
//     if (x !== Aa1.a || x !== Bb1.b) {
//
//     }
// }

declare enum A {
    a=1,
    b,
    c=2
}
