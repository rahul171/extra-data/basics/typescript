type Rs = {
    a: number;
    b: number;
}

type A = {
    (x: number, y: number, z?: undefined): Rs;
}

type AA = A & {
    q: (x: string, y: boolean) => boolean;
};

type AAA = AA & {
    a: number,
    b: string
};

const a: AAA = (a, b, c) => {
    return { a: 11, b: 22 };
};

a.q = (a, b) => {
    return true;
};

a.a = 11;
a.b = '33';

const val = a(1, 222222);
