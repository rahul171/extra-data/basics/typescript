type A = {
    a: number;
    b: string;
};

type AA = (x: number, y: string) => string;

interface B {
    a: number;
    b: string;
}

interface BB {
    (x?: number, y?: string): string;
}

const a: A = {
    a: 11,
    b: 'ss'
};

const aa: AA = (a, b) => {
    console.log(a);
    console.log(b);
    return 'ss';
}

aa(1, 'aa');

const b: B = {
    a: 1,
    b: 'ss'
};

const bb: BB = (a = 1, b = 'ss') => {
    console.log(a);
    console.log(b);
    return '11';
}

bb();
