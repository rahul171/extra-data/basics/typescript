interface A {
    (x: number, y: string, z: unknown): unknown;
}

interface AA extends A {
    q: (x: boolean, y: boolean) => boolean;
}

interface AAA extends AA {
    a: number;
    b: boolean;
}

type Rs = {
    a: number;
    b: number;
}

const a: AAA = (a, b, c) => {
    return JSON.parse(`{"d":"e"}`);
};

const b = a(11, 'aa', 22 as number) as Rs;

console.log(b);

// const b = ((a, b) => {
//     return JSON.parse(`{"a":"b"}`);
// })() as Rs;

a.q = (a, b) => {
    return a && b;
}

a.a = 11;
a.b = true;

const jsonParser = (jsonString: string): unknown => {
    return JSON.parse(jsonString);
};

// const b = jsonParser('{"a":"b"}');

// console.log(b.a);

type Abcd = A | Rs;

const abcd: Abcd = {
    a: 11,
    b: 22
}

const abcd1: Abcd = (a, b, c) => {
    return jsonParser(`{"a": "b"}`);
};

const abcdR1 = abcd1(11, '22', 33) as { a: string, b: number };

type Rss = Rs | { c: number };

const rss: Rss = {
    a: 11,
    b: 22,
    c: 44
};
